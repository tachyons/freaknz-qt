#ifndef ASCIITOUNICODECONVERTER_H
#define ASCIITOUNICODECONVERTER_H
#include <QString>
#include <QFileInfo>


class asciiToUnicodeConverter
{
public:
    asciiToUnicodeConverter();
    QString asciiToUnicodeConverterFunction(QString, QString);

private:
    QString path = QFileInfo(".").absolutePath() +"/freaknz-qt/";
};

#endif // ASCIITOUNICODECONVERTER_H
