#include "asciitounicodeconverter.h"
#include <QDebug>
#include <QFile>
asciiToUnicodeConverter::asciiToUnicodeConverter()
{

}

QString asciiToUnicodeConverter::asciiToUnicodeConverterFunction(QString asciiString, QString fontName) {


    QMap <QString,QString> preBase;

    preBase["േ"] = "True";
    preBase["െ"] = "True";
    preBase["ൈ"] = "True";
    preBase["ോ"] = "True";
    preBase["ൊ"] = "True";
    preBase["ൌ"] = "True";
    preBase["്ര"] = "True";

    //setting map for postbase elements

    QMap <QString,QString> postBase;

    postBase["്യ"] = "True";
    postBase["്വ"] = "True";


    QString x;

    QFile nfile(path+"map/"+fontName+".map");
//qDebug()<<QFileInfo(".").absolutePath() +"/freaknz-qt/map/"+nmapFile+".map";

    //Making a QMap to map ascii to unicode character from the input mapfile

    QMap<QString, QString> mapString;


    if (!nfile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug()<<"Mapping file not found. Can't Convert the text";
    }
    else {

        while (!nfile.atEnd()) {
            QString line = nfile.readLine();
            if(line.contains("=")){
                QString lhs = line.split("=")[0];
                QString rhs = line.split("=")[1];
                lhs = lhs.simplified();
                rhs = rhs.simplified();
                mapString[lhs]=rhs;
            }
        }
        nfile.close();

    }




    for (QString::size_type i = 0; i < asciiString.length(); i++){ //cutting the input ascii string to single letters


        QString p = mapString[asciiString.mid(i,1)];
        QString stripedCharacter = asciiString.mid(i,1);
        QString::size_type j = i+1;
        QString::size_type k = i+2;

        QString symbols(" 	,;.:?01234\'\"56789\n");


        QString mixCharctors("CDH");

        //letting the symbols go unharmed by conversion
        if (symbols.contains(stripedCharacter)){
            x = x.append(stripedCharacter);
            } else if (p.isNull()) {
                x.append(asciiString.mid(i,1));

    }

        //to deal with "ഈ,ഊ,ഔ,ഓ"
        else if (mixCharctors.contains(stripedCharacter)){

            QString nextCharactor = asciiString.mid(j,1);
            if (nextCharactor =="u"){
                if (stripedCharacter == "C") {
                    x = x.append(mapString["Cu"]); //ഈ
                } else if (stripedCharacter == "D") {
                    x = x.append(mapString["Du"]); //ഊ
                } else if (stripedCharacter == "H") {
                    x = x.append(mapString["Hu"]); //ഔ
                }
                i++;
            } else if (nextCharactor == "m") {
                if (stripedCharacter == "H") {
                    x = x.append(mapString["Hm"]); //ഓ
                }
                else if (stripedCharacter == "t") {
                    x = x.append(mapString["tm"]); //ോ
                }
                i++;
            } else {
                x = x.append(mapString[stripedCharacter]);
            }
        }

        else if (stripedCharacter == "s" && asciiString.mid(j,1) == "F") {
            x = x.append(mapString["sF"]); //ഐ
            i++;
        }

        else if (stripedCharacter == "s" && asciiString.mid(j,1) == "s") {
            x = x.append(mapString[asciiString.mid(k,1)]+mapString["ss"]);//ൈ
            i=i+2;
        }


        else if (preBase[p]=="True"){ //Checking for prebase character
            QString  preBaseCharactor = p;
            QString mainCharactor = mapString[asciiString.mid(j,1)];
            QString q = mapString[asciiString.mid(k,1)];

            if (postBase[q]=="True") { //checking if next character is postbase

                QString postBaseCharacter = q;
                x = x.append(mainCharactor+postBaseCharacter+preBaseCharactor); //always add postbase character near to main character
                i=i+2;                                                          //before the prebase prebase character
            }

            else {
                if (symbols.contains(asciiString.mid(k,1))){
                    x = x.append(mainCharactor+preBaseCharactor+asciiString.mid(k,1));
                    i=i+2;
                }else {
                    if (asciiString.mid(j,1)=="{") {
                        x = x.append(q+mapString["{"]+preBaseCharactor); //to deal with ്ര
                        i=i+2;
                    }

                    else {
                        x=x.append(mainCharactor+preBaseCharactor);
                        i++;
                    }
                }
            }
        }
        else {
            x=x.append(p);
        }

    }


    return x;
}
