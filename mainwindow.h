/*
Freaknz-qt: A QT application to convert Malayalam ASCII to Unicode format.
Copyright (C) Kannan V M 2019

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QFileInfo>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionClear_triggered();
    void on_actionUnicodeEditor_triggered();
    void on_actionCleanText_triggered();
    void on_actionFontChange(int);
    void on_actionASCIIEditor_triggered();
    void on_actionOpen_triggered();
    void on_actionPaste_triggered();
    void on_actionPrint_triggered();
    void on_actionSave_triggered();
    void on_actionSave_As_triggered();
    void on_actionCopy_triggered();
    void on_actionExit_triggered();
    void on_actionAsciiToUnicodeConvert_triggered();
    void on_actionUndo_triggered();
    void on_actionAbout_triggered();
    void on_actionRedo_triggered();
    void on_actionFull_Screen_triggered();
    void on_actionNormal_Screen_triggered();
    void on_actionFontSizeChange(int);
    void on_actionCharChange();
//    void on   _actionPreferences_triggered();
//    void on_test();

private:
    Ui::MainWindow *ui;
    QString fileNameAscii;
    QString fileNameUnicode;
    QString wordCnt = "Word Count: 0";
    QString characterCnt = "Letter Count: 0";
    QString samW;
    QString hasML;
    QString path = QFileInfo(".").absolutePath() +"/freaknz-qt/";
};

#endif // MAINWINDOW_H
