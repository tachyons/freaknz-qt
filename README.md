Freaknz-qt is a application based on QT5 to convert ASCII Malayalam text to Unicode.

### Build Dependancies
qt-default  

### How to build
```
git clone https://gitlab.com/kannanvm/freaknz-qt.git
cd freaknz-qt
qmake
make
./freaknz
```
This application is released unde GPL V3.

The map files are taken from [Payyans Project](https://github.com/libindic/payyans) which are released under GPL V3.